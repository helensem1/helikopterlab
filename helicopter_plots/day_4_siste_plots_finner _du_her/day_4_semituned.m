% FOR HELICOPTER NR 3-10
% This file contains the initialization for the helicopter assignment in
% the course TTK4115. Run this file before you execute QuaRC_ -> Build 
% to build the file heli_q8.mdl.

% Oppdatert h�sten 2006 av Jostein Bakkeheim
% Oppdatert h�sten 2008 av Arnfinn Aas Eielsen
% Oppdatert h�sten 2009 av Jonathan Ronen
% Updated fall 2010, Dominik Breu
% Updated fall 2013, Mark Haring
% Updated spring 2015, Mark Haring


%%%%%%%%%%% Calibration of the encoder and the hardware for the specific
%%%%%%%%%%% helicopter
Joystick_gain_x = 0.6;
Joystick_gain_y = -0.6;


%%%%%%%%%%% Physical constants
g = 9.81; % gravitational constant [m/s^2]
l_c = 0.46; % distance elevation axis to counterweight [m]
l_h = 0.66; % distance elevation axis to helicopter head [m]
l_p = 0.175; % distance pitch axis to motor [m]
m_c = 1.92; % Counterweight mass [kg]
m_p = 0.72; % Motor mass [kg]
V_s_0 = 7.5; %equilibrium voltage 
K_f = (l_h*2*g*m_p - l_c*m_c*g)/(V_s_0*l_h);%Motor force constant
K_1 = l_p*K_f/(2*m_p*(l_p)^2); %pitch constant
K_2 = l_h*K_f/(m_c*(l_c)^2 + 2*m_p*(l_h)^2); %elevation constant
K_3 = l_h*K_f/(m_c*(l_c)^2+2*m_p*((l_h)^2 + (l_p)^2));
lamda_1 = -2.7+1i; %pole 1
lamda_2 = -2.7-1i; %pole 2
K_pp = lamda_1*lamda_2/K_1; %Pitch propotional constant
K_pd = (-lamda_1-lamda_2)/K_1; %Pitch derivative constant


A = [0 1 0; 0 0 0; 0 0 0];
B = [0 0; 0 K_1; K_2 0];

%K = place (A,B,p);
%Q = [100 0 0; 0 20 0; 0 0 50];
%R = [0.1 0; 0 0.6];
%K = lqr(A,B,Q,R); 

%%%%%%%%%%% K- matrix
k_11 = K(1,1); 
k_12 = K(1,2); 
k_13 = K(1,3); 
k_21 = K(2,1); 
k_22 = K(2,2); 
k_23 = K(2,3); 

F = [k_11 k_13; k_21 k_23];

Q_int = [100 0 0 0 0; 0 20 0 0 0; 0 0 50 0 0; 0 0 0 30 0; 0 0 0 0 80];
R_int = [0.1 0; 0 0.6];
A_int = [0 1 0 0 0; 0 0 0 0 0; 0 0 0 0 0; 1 0 0 0 0; 0 0 1 0 0];
B_int = [0 0; 0 K_1; K_2 0; 0 0; 0 0];
K_int = lqr(A_int,B_int, Q_int, R_int);

p_k = eig(A_int - B_int*K_int);

    
k_11_int = K_int(1,1); 
k_12_int = K_int(1,2); 
k_13_int = K_int(1,3);
k_14_int = K_int(1,4);
k_15_int = K_int(1,5);
k_21_int = K_int(2,1); 
k_22_int = K_int(2,2); 
k_23_int = K_int(2,3);
k_24_int = K_int(2,4);
k_25_int = K_int(2,5);

%%% Day 3 %%%%%
PORT = 8;
%40 magnitude 
p = [-40; - 39.85 + 3.49i;  - 39.85 - 3.49i; -39.39 + 6.95i; -39.39 - 6.95i];
A_l = [0 1 0 0 0; 0 0 0 0 0; 0 0 0 1 0; 0 0 0 0 0; K_3 0 0 0 0];
B_l = [0 0; 0 K_1; 0 0; K_2 0; 0 0];
C_l = [1 0 0 0 0; 0 1 0 0 0; 0 0 1 0 0; 0 0 0 1 0; 0 0 0 0 1];
L = transpose(place(transpose(A_l),transpose(C_l),p));



%%% Day 4 %%%%


datastruct1 = load('day_4/noise_laying_heli.mat');
datastruct2 = load('day_4/noise_flying_helicopter.mat');

y1 = [datastruct1.p(1,:); datastruct1.p_dot(1,:); datastruct1.e(1,:); datastruct1.e_dot(1,:); datastruct1.lamda_dot(1,:)];
y2 = [datastruct2.p(1,:); datastruct2.p_dot(1,:); datastruct2.e(1,:); datastruct2.e_dot(1,:); datastruct2.lamda_dot(1,:)];


R_d1 = cov(transpose(y1)); %R_d lying helicopter
%R_d2 = cov(transpose(y2)); %R_d flying helicopter
R_d2 = [0.000242183954971290 0 0 0 0;0 0.00112556348980800 0 0 0;0 0 0.0107551532510785 0 0;0 0 0 0.000572133240831746 0;0 0 0 0 0.000160542341595206];

%The noise was much bigger when the helicopter was flying, therefore R_d2 is
%much larger

%p_4 = [-8;-11;-15;-5;-10;-7];
A_4 = [0 1 0 0 0 0; 0 0 0 0 0 0; 0 0 0 1 0 0; 0 0 0 0 0 0;0 0 0 0 0 1; K_3 0 0 0 0 0];
B_4 = [0 0; 0 K_1; 0 0; K_2 0; 0 0; 0 0];
C_4 = [1 0 0 0 0 0; 0 1 0 0 0 0; 0 0 1 0 0 0; 0 0 0 1 0 0; 0 0 0 0 0 1];

%sysd = c2d(0.002);
system=ss(A_4, B_4, C_4,0);
diskret_system = c2d(system, 0.002);
[A_d, B_d, C_d] = ssdata(diskret_system);
I_d = [1 0 0 0 0 0; 0 1 0 0 0 0; 0 0 1 0 0 0; 0 0 0 1 0 0;0 0 0 0 1 0; 0 0 0 0 0 1];
Q_d = [0.000001 0 0 0 0 0; 0 0.00001 0 0 0 0; 0 0 0.00001 0 0 0; 0 0 0 0.00001 0 0;0 0 0 0 1 0; 0 0 0 0 0 0.00001];


