% FOR HELICOPTER NR 3-10
% This file contains the initialization for the helicopter assignment in
% the course TTK4115. Run this file before you execute QuaRC_ -> Build 
% to build the file heli_q8.mdl.

% Oppdatert h�sten 2006 av Jostein Bakkeheim
% Oppdatert h�sten 2008 av Arnfinn Aas Eielsen
% Oppdatert h�sten 2009 av Jonathan Ronen
% Updated fall 2010, Dominik Breu
% Updated fall 2013, Mark Haring
% Updated spring 2015, Mark Haring


%%%%%%%%%%% Calibration of the encoder and the hardware for the specific
%%%%%%%%%%% helicopter
Joystick_gain_x = 0.6;
Joystick_gain_y = -1;


%%%%%%%%%%% Physical constants
g = 9.81; % gravitational constant [m/s^2]
l_c = 0.46; % distance elevation axis to counterweight [m]
l_h = 0.66; % distance elevation axis to helicopter head [m]
l_p = 0.175; % distance pitch axis to motor [m]
m_c = 1.92; % Counterweight mass [kg]
m_p = 0.72; % Motor mass [kg]
V_s_0 = 7.5; %equilibrium voltage 
K_f = (l_h*2*g*m_p - l_c*m_c*g)/(V_s_0*l_h);%Motor force constant
K_1 = l_p*K_f/(2*m_p*(l_p)^2); %pitch constant
K_2 = l_h*K_f/(m_c*(l_c)^2 + 2*m_p*(l_h)^2); %elevation constant
lamda_1 = -2.7+1i; %pole 1
lamda_2 = -2.7-1i; %pole 2
K_pp = lamda_1*lamda_2/K_1; %Pitch propotional constant
K_pd = (-lamda_1-lamda_2)/K_1; %Pitch derivative constant

%%%%%%%%%%% K- matrix
k_11 = 0; 
k_12 = 0; 
k_13 = 11.7584097859327; 
k_21 = 15.686353211009152; 
k_22 = 10.217889908256867; 
k_23 = 0; 

A = [0 1 0; 0 0 0; 0 0 0];
B = [0 0; 0 K_1; K_2 0];
p = [lamda_1;lamda_2;-1];
K = place (A,B,p);
