% FOR HELICOPTER NR 3-10
% This file contains the initialization for the helicopter assignment in
% the course TTK4115. Run this file before you execute QuaRC_ -> Build 
% to build the file heli_q8.mdl.

% Oppdatert h�sten 2006 av Jostein Bakkeheim
% Oppdatert h�sten 2008 av Arnfinn Aas Eielsen
% Oppdatert h�sten 2009 av Jonathan Ronen
% Updated fall 2010, Dominik Breu
% Updated fall 2013, Mark Haring
% Updated spring 2015, Mark Haring


%%%%%%%%%%% Calibration of the encoder and the hardware for the specific
%%%%%%%%%%% helicopter
Joystick_gain_x = 0.6;
Joystick_gain_y = -1;


%%%%%%%%%%% Physical constants
g = 9.81; % gravitational constant [m/s^2]
l_c = 0.46; % distance elevation axis to counterweight [m]
l_h = 0.66; % distance elevation axis to helicopter head [m]
l_p = 0.175; % distance pitch axis to motor [m]
m_c = 1.92; % Counterweight mass [kg]
m_p = 0.72; % Motor mass [kg]
V_s_0 = 7.5; %equilibrium voltage 
K_f = (l_h*2*g*m_p - l_c*m_c*g)/(V_s_0*l_h);%Motor force constant
K_1 = l_p*K_f/(2*m_p*(l_p)^2); %pitch constant
K_2 = l_h*K_f/(m_c*(l_c)^2 + 2*m_p*(l_h)^2); %elevation constant
K_3 = l_h*K_f/(m_c*(l_c)^2+2*m_p*((l_h)^2 + (l_p)^2));
lamda_1 = -2.7+1i; %pole 1
lamda_2 = -2.7-1i; %pole 2
K_pp = lamda_1*lamda_2/K_1; %Pitch propotional constant
K_pd = (-lamda_1-lamda_2)/K_1; %Pitch derivative constant


A = [0 1 0; 0 0 0; 0 0 0];
B = [0 0; 0 K_1; K_2 0];

%K = place (A,B,p);
Q = [100 0 0; 0 20 0; 0 0 50];
R = [1 0; 0 5];
K = lqr(A,B,Q,R); 

%%%%%%%%%%% K- matrix
k_11 = K(1,1); 
k_12 = K(1,2); 
k_13 = K(1,3); 
k_21 = K(2,1); 
k_22 = K(2,2); 
k_23 = K(2,3); 

F = [k_11 k_13; k_21 k_23];

Q_int = [100 0 0 0 0; 0 20 0 0 0; 0 0 50 0 0; 0 0 0 30 0; 0 0 0 0 0.1];
R_int = [1 0; 0 5];
A_int = [0 1 0 0 0; 0 0 0 0 0; 0 0 0 0 0; 1 0 0 0 0; 0 0 1 0 0];
B_int = [0 0; 0 K_1; K_2 0; 0 0; 0 0];
K_int = lqr(A_int,B_int, Q_int, R_int);

    
k_11_int = K_int(1,1); 
k_12_int = K_int(1,2); 
k_13_int = K_int(1,3);
k_14_int = K_int(1,4);
k_15_int = K_int(1,5);
k_21_int = K_int(2,1); 
k_22_int = K_int(2,2); 
k_23_int = K_int(2,3);
k_24_int = K_int(2,4);
k_25_int = K_int(2,5);

%%% Day 3 %%%%%
PORT = 4;

p = [-2;-4 ;-6;-7;-1];
A_l = [0 1 0 0 0; 0 0 0 0 0; 0 0 0 1 0; 0 0 0 0 0; K_3 0 0 0 0];
B_l = [0 0; 0 K_1; 0 0; K_2 0; 0 0];
C_l = [1 0 0 0 0; 0 1 0 0 0; 0 0 1 0 0; 0 0 0 1 0; 0 0 0 0 1];
L = transpose(place(transpose(A_l),transpose(C_l),p));


