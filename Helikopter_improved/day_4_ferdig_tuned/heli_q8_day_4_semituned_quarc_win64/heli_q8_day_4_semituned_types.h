/*
 * heli_q8_day_4_semituned_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "heli_q8_day_4_semituned".
 *
 * Model version              : 1.101
 * Simulink Coder version : 8.9 (R2015b) 13-Aug-2015
 * C source code generated on : Mon Nov 09 01:03:40 2020
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_heli_q8_day_4_semituned_types_h_
#define RTW_HEADER_heli_q8_day_4_semituned_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#include "zero_crossing_types.h"

/* Parameters (auto storage) */
typedef struct P_heli_q8_day_4_semituned_T_ P_heli_q8_day_4_semituned_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_heli_q8_day_4_semitun_T RT_MODEL_heli_q8_day_4_semitu_T;

#endif                                 /* RTW_HEADER_heli_q8_day_4_semituned_types_h_ */
